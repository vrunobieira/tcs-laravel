<?php
/**
 * Displays a dismissable notice panel 
 * 
 * @param string $type Panel type: info | warning | danger
 * @param array $messages An array of messages
 */
?>

@if ($messages)
  <div class="row js-click-to-dismiss" style="cursor: pointer;">
    <div class="col-md-12">
      <div class="callout callout-{{ $type }}">
        @foreach ($messages as $message)
          <div><b>{{ $message }}</b></div>
        @endforeach
      </div>
    </div>
  </div>

  <script type="text/javascript">
    function clickToDismiss() {
      $('.js-click-to-dismiss').on('click', function() {
        $(this).fadeOut(500);
      });
    }

    @if (!Request::ajax())
      document.addEventListener('DOMContentLoaded', clickToDismiss);
    @else
      clickToDismiss();
    @endif
  </script>
@endif
