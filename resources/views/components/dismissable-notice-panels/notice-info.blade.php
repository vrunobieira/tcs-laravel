<?php
/**
 * Displays a dismissable info notice panel
 * 
 * @param string $type Panel type: info | warning | danger
 * @param array $messages An array of messages
 */
?>

@includeWhen ($messages, 'components.dismissable-notice-panels.notice-by-type', array(
    'type' => 'info',
    'messages' => $messages
))