<?php
/**
 * Displays a dismissable warning notice panel
 * 
 * @param array $messages An array of messages
 */
?>

@includeWhen ($messages, 'components.dismissable-notice-panels.notice-by-type', array(
    'type' => 'warning',
    'messages' => $messages
))