<?php
/**
 * Displays a dismissable info notice panel
 * 
 * @param array $messages An array of messages
 */
?>

@includeWhen ($messages, 'components.dismissable-notice-panels.notice-by-type', array(
    'type' => 'danger',
    'messages' => $messages
))