<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

  <!-- START @HEAD -->
  <head>
    <!-- START @META SECTION -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="TCS Industrial | Painel Administrativo">
    <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
    <meta name="author" content="Vrunobieira">
    
    <title>TCS Industrial | Painel Administrativo</title>
    <!--/ END META SECTION -->

    <!-- START @FAVICONS -->
    <!-- 
    <link href="http://themes.djavaui.com/blankon-fullpack-admin-theme/img/ico/html/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
    <link href="http://themes.djavaui.com/blankon-fullpack-admin-theme/img/ico/html/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
    <link href="http://themes.djavaui.com/blankon-fullpack-admin-theme/img/ico/html/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
    <link href="http://themes.djavaui.com/blankon-fullpack-admin-theme/img/ico/html/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed">
    <link href="http://themes.djavaui.com/blankon-fullpack-admin-theme/img/ico/html/apple-touch-icon.png" rel="shortcut icon">
    -->
    <!--/ END FAVICONS -->

    <!-- START @FONT STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
    <!--/ END FONT STYLES -->

    <!-- START @GLOBAL MANDATORY STYLES -->
    <link href="/css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!--/ END GLOBAL MANDATORY STYLES -->

    <!-- START @PAGE LEVEL STYLES -->
    <link href="/css/fontawesome/css/font-awesome.css" rel="stylesheet">
    <link href="/css/animate.css/animate.css" rel="stylesheet">
    <!--/ END PAGE LEVEL STYLES -->

    <!-- START @THEME STYLES -->
    <link href="/css/reset.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <link href="/css/components.css" rel="stylesheet">
    <link href="/css/plugins.css" rel="stylesheet">
    <link href="/css/theme/default.theme.css" rel="stylesheet" id="theme">
    <link href="/css/pages/sign.css" rel="stylesheet">
    <!--/ END THEME STYLES -->

    <!-- START @IE SUPPORT -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv/html5shiv.js"></script>
    <script src="/js/respond-minmax/respond.js"></script>
    <![endif]-->
    <!--/ END IE SUPPORT -->
  </head>
  <!--/ END HEAD -->

  <body class='page-sound'>

    <!--[if lt IE 9]>
    <p class="upgrade-browser">Seu navegador est&aacute; <strong>desatualizado</strong>. Por favor, atualize seu navegador.</p>
    <![endif]-->

    @yield('content')

    <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
    <!-- START @CORE PLUGINS -->
    <script src="/js/jquery/jquery.min.js"></script>
    <script src="/js/jquery-cookie/jquery.cookie.js"></script>
    <script src="/js/bootstrap/bootstrap.min.js"></script>
    <script src="/js/jquery-nicescroll/jquery.nicescroll.min.js"></script>
    <script src="/js/jquery-easing-original/jquery.easing.1.3.min.js"></script>
    <!--/ END CORE PLUGINS -->
     
    <!-- START @PAGE LEVEL SCRIPTS -->
    <script src="/js/app/blankon.layout.js"></script>

    <!--/ END PAGE LEVEL SCRIPTS -->
    <!--/ END JAVASCRIPT SECTION -->
  </body>
  <!--/ END BODY -->
</html>
