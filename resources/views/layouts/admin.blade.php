<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

  <!-- START @HEAD -->
  <head>
    <!-- START @META SECTION -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="TCS Industrial | Painel Administrativo">
    <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
    <meta name="author" content="Vrunobieira">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>TCS Industrial | Painel Administrativo</title>
    <!--/ END META SECTION -->

    <!-- START @FONT STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
    <!--/ END FONT STYLES -->

    <!-- START @GLOBAL MANDATORY STYLES -->
    <link href="/css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!--/ END GLOBAL MANDATORY STYLES -->

    <!-- START @PAGE LEVEL STYLES -->
    <link href="/css/fontawesome/css/font-awesome.css" rel="stylesheet">
    <link href="/css/animate.css/animate.css" rel="stylesheet">
    <!--/ END PAGE LEVEL STYLES -->

    <!-- START @THEME STYLES -->
    <link href="/css/reset.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <link href="/css/components.css" rel="stylesheet">
    <link href="/css/plugins.css" rel="stylesheet">
    <link href="/css/theme/default.theme.css" rel="stylesheet" id="theme">
    <!--/ END THEME STYLES -->

    <!-- START @IE SUPPORT -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv/html5shiv.js"></script>
    <script src="/js/respond-minmax/respond.js"></script>
    <![endif]-->
    <!--/ END IE SUPPORT -->

    <!-- CUSTOM CSS FOR MODULES [BEGIN] -->
    @stack('css-modules')
    <!-- CUSTOM CSS FOR MODULES [END] -->
  </head>
  <!--/ END HEAD -->

  <body>
    <!--[if lt IE 9]>
    <p class="upgrade-browser">Seu navegador est&aacute; <strong>desatualizado</strong>. Por favor, atualize seu navegador.</p>
    <![endif]-->

    <!-- START @WRAPPER -->
    <section id="wrapper">

      <!-- START @HEADER -->
      <header id="header">

        <!-- Start header left -->
        <div class="header-left">
          <!-- Start offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
          <div class="navbar-minimize-mobile left">
            <i class="fa fa-bars"></i>
          </div>
          <!--/ End offcanvas left -->

          <!-- Start navbar header -->
          <div class="navbar-header">

            <!-- Start brand -->
            <a class="navbar-brand" href="javascript: void(0);">
              <img 
                class="logo" 
                src="http://via.placeholder.com/175x50"
                alt="logo empresa" />
            </a><!-- /.navbar-brand -->
            <!--/ End brand -->

          </div><!-- /.navbar-header -->
          <!--/ End navbar header -->

          <div class="clearfix"></div>
        </div><!-- /.header-left -->
        <!--/ End header left -->

        <!-- Start header right -->
        <div class="header-right">
          <!-- Start navbar toolbar -->
          <div class="navbar navbar-toolbar">

            <!-- Start left navigation -->
            <ul class="nav navbar-nav navbar-left">

              <!-- Start sidebar shrink -->
              <li class="navbar-minimize">
                <a href="javascript:void(0);" title="Minimize sidebar">
                  <i class="fa fa-bars"></i>
                </a>
              </li>
              <!--/ End sidebar shrink -->

              <!-- section para o formulario de pesquisa [begin] -->
              @section('search-form')
              @show
              <!-- section para o formulario de pesquisa [end] -->

            </ul><!-- /.navbar-left -->
            <!--/ End left navigation -->

            <!-- Start right navigation -->
            <ul class="nav navbar-nav navbar-right"><!-- /.nav navbar-nav navbar-right -->

            <!-- Start profile -->
            <li class="dropdown navbar-profile">
              <a href="javascript: void(0);" class="dropdown-toggle" data-toggle="dropdown">
                <span class="meta">
                  <span class="avatar"><img src="http://via.placeholder.com/30x30" class="img-circle" alt="admin"></span>
                  <span class="text hidden-xs hidden-sm text-muted">{{ Auth::user()->name }}</span>
                  <span class="caret"></span>
                </span>
              </a>
              <!-- Start dropdown menu -->
              <ul class="dropdown-menu animated flipInX">
                <li class="dropdown-header">
                  Minha Conta
                </li>
                <li class="divider"></li>
                <li>
                  <a href="/logout"><i class="fa fa-sign-out"></i>Sair</a>
                </li>
              </ul>
              <!--/ End dropdown menu -->
            </li><!-- /.dropdown navbar-profile -->
            <!--/ End profile -->

            </ul><!-- /.navbar-right -->
            <!--/ End right navigation -->

          </div><!-- /.navbar-toolbar -->
          <!--/ End navbar toolbar -->
        </div><!-- /.header-right -->
        <!--/ End header left -->

      </header> <!-- /#header -->
      <!--/ END HEADER -->

      <!-- SIDEBAR LEFT -->
      <aside id="sidebar-left" class="sidebar-circle">

        <!-- Start left navigation - profile shortcut -->
        <div class="sidebar-content">
          <div class="media">
            <a class="pull-left avatar" href="/admin/profile">
              <img src="http://via.placeholder.com/40x40" alt="admin">
              <i class="online"></i>
            </a>
            <div class="media-body">
              <h4 class="media-heading">Ol&aacute;, <span>{{ Auth::user()->name }}</span></h4>
            </div>
          </div>
        </div><!-- /.sidebar-content -->
        <!--/ End left navigation -  profile shortcut -->

        <!-- Start left navigation - menu -->
        <ul class="sidebar-menu">
          <li>
            <a href="/admin/statuses">
              <span class="icon"><i class="fa fa-tags"></i></span>
              <span class="text">Status</span>
            </a>
          </li>
          <li>
            <a href="/admin/engines">
              <span class="icon"><i class="fa fa-android"></i></span>
              <span class="text">M&aacute;quinas</span>
            </a>
          </li>
          <li>
            <a href="/admin/simulator">
              <span class="icon"><i class="fa fa-cogs"></i></span>
              <span class="text">Simulador</span>
            </a>
          </li>
        </ul><!-- /.sidebar-menu -->
        <!--/ End left navigation - menu -->

      </aside><!-- /#sidebar-left -->
      <!--/ END SIDEBAR LEFT -->

      <!-- START @PAGE CONTENT -->
      <section id="page-content">

        <!-- Start page header -->
        <div class="header-content">
          <!-- section com cabecalho e breadcrumbs -->
          @section('page-header')
          @show
          <!-- /section com cabecalho e breadcrumbs -->
        </div><!-- /.header-content -->
        <!--/ End page header -->

        <!-- Start body content -->
        <div class="body-content animated fadeIn" style="min-height: 600px;">
          @yield('content')
        </div><!-- /.body-content -->
        <!--/ End body content -->

      </section>
      <!--/ END PAGE CONTENT -->

    </section><!-- /#wrapper -->
    <!--/ END WRAPPER -->

    <!-- START @BACK TOP -->
    <div id="back-top" class="animated pulse circle">
      <i class="fa fa-angle-up"></i>
    </div><!-- /#back-top -->
    <!--/ END BACK TOP -->

    <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
    <!-- START @CORE PLUGINS -->
    <script src="/js/jquery/jquery.min.js"></script>
    <script src="/js/jquery-cookie/jquery.cookie.js"></script>
    <script src="/js/bootstrap/bootstrap.min.js"></script>
    <script src="/js/jquery-nicescroll/jquery.nicescroll.min.js"></script>
    <script src="/js/jquery-easing-original/jquery.easing.1.3.min.js"></script>
    <!--/ END CORE PLUGINS -->
     
    <!-- START @PAGE LEVEL SCRIPTS -->
    <script src="/js/app/app.js"></script>
    <script src="/js/app/blankon.layout.js"></script>

    <!-- MODULES JAVASCRIPT FILES [BEGIN] -->
    @stack('javascript-modules')
    <!-- MODULES JAVASCRIPT FILES [END] -->

    <!--/ END PAGE LEVEL SCRIPTS -->
    <!--/ END JAVASCRIPT SECTION -->
  </body>
  <!--/ END BODY -->
</html>
