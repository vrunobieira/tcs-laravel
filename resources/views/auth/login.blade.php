<!-- default layout theme -->
@extends('layouts.authenticate')

<!-- page contents -->
@section('content')

<!-- START @SIGN WRAPPER -->
<div id="sign-wrapper">

    <!-- Login form -->
    <form class="sign-in form-horizontal shadow rounded no-overflow" action="/login" method="post">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">        

        <div class="sign-header">
            <div class="form-group">
                <div class="sign-text">
                    <span>Bem vindo a Area 51</span>
                </div>
            </div><!-- /.form-group -->
        </div><!-- /.sign-header -->
        <div class="sign-body">
            <div class="form-group">
                <div class="input-group input-group-lg rounded no-overflow">
                    <input type="text" class="form-control input-sm" placeholder="Digite seu Email" name="email">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                </div>
            </div><!-- /.form-group -->
            <div class="form-group">
                <div class="input-group input-group-lg rounded no-overflow">
                    <input type="password" class="form-control input-sm" placeholder="Senha" name="password">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                </div>
            </div><!-- /.form-group -->
        </div><!-- /.sign-body -->
        <div class="sign-footer">
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="ckbox ckbox-theme">
                            <input id="rememberme" type="checkbox" name="remember">
                            <label for="rememberme" class="rounded">Lembrar meu login</label>
                        </div>
                    </div>
                </div>
            </div><!-- /.form-group -->
            <div class="form-group">
                <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded" id="login-btn">Entrar</button>
            </div><!-- /.form-group -->
        </div><!-- /.sign-footer -->
    </form><!-- /.form-horizontal -->
    <!--/ Login form -->

</div><!-- /#sign-wrapper -->
<!--/ END SIGN WRAPPER -->
@endsection