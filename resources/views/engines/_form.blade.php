<?php
/**
 * @var \App\Models\Engine $engine
 * @var Status[] $statuses
 */
?>

<div class="panel rounded shadow">
  
  @includeWhen($engine->has_errors, 'components.dismissable-notice-panels.notice-warning', array(
    'messages' => $engine->error_messages
  )) 

  <div class="panel-body">
    <div class="form-group">
      <label class="col-sm-3 control-label">Descri&ccedil;&atilde;o:</label>
      <div class="col-sm-7">
        <input 
          type="text" 
          name='engine[description]' 
          class="form-control input-md" 
          placeholder="Nome da máquina" 
          value="{{ $engine->description }}" />
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Status:</label>
      <div class="col-sm-7">
        <select class="form-control input-md" name='engine[status_id]'>
          <option value="">Selecione uma op&ccedil;&atilde;o</option>
          @foreach ($statuses as $status)
            <option 
              value="{{ $status->id }}"
              @php echo ($status->id == $engine->status_id) ? ' selected="selected" ' : '' @endphp>
              {{ $status->description }} 
            </option>
          @endforeach
        </select>
      </div>
    </div>
  </div>
</div>

<div class="form-footer">
  <div class="col-sm-offset-3">
    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
    <a href="/admin/engines" class="btn btn-primary"><i class="fa fa-reply"></i> Voltar</a>
  </div>
</div>