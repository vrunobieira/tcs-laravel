<?php
/**
 * @var \App\Models\Engine $engine
 */
?>

@extends('layouts.admin')

@include('engines.page-header')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel rounded shadow">
      <div class="panel-body">
        <div class="table-responsive" style="margin-top: -1px;">
          <table class="table table-striped table-success">
            <tbody>
              <tr>
                <th>Id</th>
                <td>{{ $engine->id }}</td>
              </tr>
              <tr>
                <th width="18%">Descri&ccedil;&atilde;o</th>
                <td>{{ $engine->description }}</td>
              </tr>
              <tr>
                <th>Status</th>
                <td>{{ $engine->status_name }}</td>
              </tr>
              <tr>
                <th>Criado em</th>
                <td>{{ $engine->created_at_br }}</td>
              </tr>
              <tr>
                <th>Atualizado em</th>
                <td>{{ $engine->updated_at_br }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="form-footer">
        <div>
          <a href="/admin/engines" class="btn btn-primary"><i class="fa fa-reply"></i> Voltar</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection