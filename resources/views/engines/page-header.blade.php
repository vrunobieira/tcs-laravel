@section('page-header')

<h2>
  <a href="/admin/engine/create"><i class="fa fa-file-o"></i></a>
  M&aacute;quinas<span>Cadastro e gerenciamento de m&aacute;quinas</span>
</h2>

<div class="breadcrumb-wrapper hidden-xs">
  <span class="label">Voc&ecirc; est&aacute; aqui:</span>
  <ol class="breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="javascript: void(0);">Simulador de Eventos</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li class="active">M&aacute;quinas</li>
  </ol>
</div>
@endsection