<?php
/**
 * @var array $filter
 * @var Status[] $statuses
 */
?>
@section('search-form')

<li class="navbar-search">
  <a href="#" class="trigger-search">
    <i class="fa fa-search"></i>
  </a>
  <form class="navbar-form" action="/admin/engines">
    <div class="form-group has-feedback">
      <select name="filter[status]" class="form-control">
        <option value="">Status</option>
        @foreach ($statuses as $status)
          <option 
            value="{{ $status->id }}" 
            @php echo ($status->id == $filter['status']) ? ' selected="selected" ' : '' @endphp> 
            {{ $status->description }} 
          </option>  
        @endforeach
      </select>
    </div>
    <div class="form-group has-feedback">
      <input type="text" name="filter[search]" value="{{ $filter['search'] }}" class="form-control rounded" placeholder="Pesquisa por descri&ccedil;&atilde;o">
      <button type="submit" class="btn btn-theme fa fa-search form-control-feedback rounded"></button>
    </div>
  </form>
</li>
@endsection