<?php
/**
 * @var \App\Models\Engine $engine
 * @var Status[] $statuses
 */
?>

<!-- default layout theme -->
@extends('layouts.admin')

<!-- header and breadcrumbs -->
@include('engines.page-header')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel rounded shadow no-overflow">
      <div class="panel-body no-padding">
        <form class="form-horizontal form-bordered" role="form" method="post" action="/admin/engine/update/{{ $engine->id }}">
          {{ csrf_field() }}

          @include('engines._form')
        </form>
      </div>
    </div>
  </div>
</div>
@endsection