<?php
/**
 * @var \Illuminate\Pagination\LengthAwarePaginator $collection (Engine)
 * @var array $successMessages
 * @var array $dangerMessages
 * @var array $filter
 * @var Status[] $statuses
 */ 
?>

<!-- default layout theme -->
@extends('layouts.admin')

<!-- header and breadcrumbs -->
@include('simulator.page-header')

@section('content')
  <!-- success messages component -->
  @includeWhen($successMessages, 'components.dismissable-notice-panels.notice-info', array(
    'messages' => $successMessages
  ))

  <!-- danger messages component -->
  @includeWhen($dangerMessages, 'components.dismissable-notice-panels.notice-danger', array(
    'messages' => $dangerMessages
  )) 

  @if (count($collection))
    @foreach ($collection as $row)
      <div 
        class="col-lg-3 col-md-3 col-sm-6 col-xs-6 js-engine_container" 
        data-id="{{ $row->engine_id }}"
        data-frequency_ms="{{ $row->frequency_ms }}">
        <div class="panel rounded shadow">
          <div class="panel-body">
            <ul class="inner-all list-unstyled">
              <li class="text-center">
                <h4 class="text-capitalize">{{ $row->engine_description }}</h4>
              </li>
              <li>
                <ul class="list-group no-margin br-3">
                  <li class="list-group-item">
                    <i class="fa fa-tag mr-5"></i><span class="js-engine_status_name">{{ $row->engine_status_name }}</span>
                  </li>
                  <li class="list-group-item">
                    <i class="fa fa-refresh mr-5"></i>Atualiza&ccedil;&atilde;o a cada {{ $row->frequency }} segundos
                  </li>
                  <li class="list-group-item">
                    <i class="fa fa-globe mr-5"></i>&Uacute;ltima atualiza&ccedil;&atilde;o: <span class="js-engine_updated_at">{{ $row->engine_updated_at }}</span>
                  </li>
                </ul>
              </li>
              <li class="text-center">
                <a href="/admin/simulator/update/{{ $row->id }}" class="btn btn-primary btn-xs" title="Editar Registro"><i class="fa fa-pencil"></i></a>
                <a href="/admin/simulator/delete/{{ $row->id }}" class="btn btn-danger btn-xs js-confirm-to-remove" title="Remover Registro"><i class="fa fa-trash-o"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    @endforeach
  @else 
    <b>N&atilde;o h&aacute; eventos de status configurado para as m&aacute;quinas<b>
  @endif
@endsection

@push('javascript-modules')
  <script src="/js/modules/simulator/index.js"></script>
@endpush