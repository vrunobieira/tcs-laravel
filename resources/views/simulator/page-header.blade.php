@section('page-header')
<h2>
  <a href="/admin/simulator/create"><i class="fa fa-file-o"></i></a>
  Simulador de Eventos<span>Configura&ccedil;&atilde;o de visualiza&ccedil;&atilde;o dos eventos atribu&iacute;dos as m&aacute;quinas</span>
</h2>

<div class="breadcrumb-wrapper hidden-xs">
  <span class="label">Voc&ecirc; est&aacute; aqui:</span>
  <ol class="breadcrumb">
    <li class="active">Simulador de Eventos</li>
  </ol>
</div>
@endsection