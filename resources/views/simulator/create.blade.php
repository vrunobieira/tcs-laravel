<?php
/**
 * @var \App\Models\Engine[] $engines
 */
?>

<!-- default layout theme -->
@extends('layouts.admin')

<!-- header and breadcrumbs -->
@include('simulator.page-header')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel rounded shadow no-overflow">
      <div class="panel-body no-padding">
        <form 
          class="form-horizontal form-bordered" 
          role="form" 
          method="post" 
          action="/admin/simulator/create">
          {{ csrf_field() }}

          @include('simulator._form')
        </form>
      </div>
    </div>
  </div>
</div>
@endsection