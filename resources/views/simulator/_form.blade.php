<?php
/**
 * @var \App\Models\Simulator $simulator
 * @var Status[] $statuses
 */
?>

<div class="panel rounded shadow">
  
  @includeWhen($simulator->has_errors, 'components.dismissable-notice-panels.notice-warning', array(
    'messages' => $simulator->error_messages
  )) 

  <div class="panel-body">
    <div class="form-group">
      <label class="col-sm-3 control-label">M&aacute;quina:</label>
      <div class="col-sm-7">
        <select class="form-control input-md" name='simulator[engine_id]'>
          <option value="">Selecione uma op&ccedil;&atilde;o</option>
          @foreach ($engines as $engine)
            <option 
              value="{{ $engine->id }}"
              @php echo ($engine->id == $simulator->engine_id) ? ' selected="selected" ' : '' @endphp>
              {{ $engine->description }} 
            </option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Periodicidade (Segundos):</label>
      <div class="col-sm-7">
        <input 
          type="text" 
          name='simulator[frequency]' 
          class="form-control input-md js-number_only" 
          placeholder="Frequencia em seg. para o evento atribuido a máquina ocorrer" 
          value="{{ $simulator->frequency }}" />
      </div>
    </div>
  </div>
</div>

<div class="form-footer">
  <div class="col-sm-offset-3">
    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
    <a href="/admin/simulator" class="btn btn-primary"><i class="fa fa-reply"></i> Voltar</a>
  </div>
</div>


<script>
  document.addEventListener('DOMContentLoaded', function() {
    $('.js-number_only').on('keypress keyup', function() {
      this.value = this.value.replace(/[^\d]+/g, "");
    })
  });
</script>