<?php
/**
 * @var \App\Models\Status $status
 */
?>

@extends('layouts.admin')

@include('statuses.page-header')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel rounded shadow">
      <div class="panel-body">
        <div class="table-responsive" style="margin-top: -1px;">
          <table class="table table-striped table-success">
            <tbody>
              <tr>
                <th>Id</th>
                <td>{{ $status->id }}</td>
              </tr>
              <tr>
                <th width="18%">Descri&ccedil;&atilde;o</th>
                <td>{{ $status->description }}</td>
              </tr>
              <tr>
                <th>Criado em</th>
                <td>{{ $status->created_at_br }}</td>
              </tr>
              <tr>
                <th>Atualizado em</th>
                <td>{{ $status->updated_at_br }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="form-footer">
        <div>
          <a href="/admin/statuses" class="btn btn-primary"><i class="fa fa-reply"></i> Voltar</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection