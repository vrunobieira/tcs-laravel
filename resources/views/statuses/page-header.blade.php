@section('page-header')
<h2>
  <a href="/admin/status/create"><i class="fa fa-file-o"></i></a>
  Status<span>Cadastro e gerenciamento de status das m&aacute;quinas</span>
</h2>

<div class="breadcrumb-wrapper hidden-xs">
  <span class="label">Voc&ecirc; est&aacute; aqui:</span>
  <ol class="breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="javascript: void(0);">Simulador de Eventos</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li class="active">Status</li>
  </ol>
</div>
@endsection