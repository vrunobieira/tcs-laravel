<?php
/**
 * @var \App\Models\Status $status
 */
?>

<!-- default layout theme -->
@extends('layouts.admin')

<!-- header and breadcrumbs -->
@include('statuses.page-header')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel rounded shadow no-overflow">
      <div class="panel-body no-padding">
        <form class="form-horizontal form-bordered" role="form" method="post" action="/admin/status/update/{{ $status->id }}">
          {{ csrf_field() }}

          @include('statuses._form')
        </form>
      </div>
    </div>
  </div>
</div>
@endsection