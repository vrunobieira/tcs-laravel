<?php
/**
 * @var \App\Models\Status $status
 */
?>
<div class="panel rounded shadow">
  @includeWhen($status->has_errors, 'components.dismissable-notice-panels.notice-warning', array(
    'messages' => $status->error_messages
  )) 

  <div class="panel-body">
    <div class="form-group">
      <label class="col-sm-3 control-label">Descri&ccedil;&atilde;o:</label>
      <div class="col-sm-7">
        <input 
          type="text" 
          name='status[description]' 
          class="form-control input-md" 
          placeholder="Descrição do Status" 
          value="{{ $status->description }}" />
      </div>
    </div>
  </div>
</div>

<div class="form-footer">
  <div class="col-sm-offset-3">
    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
    <a href="/admin/statuses" class="btn btn-primary"><i class="fa fa-reply"></i> Voltar</a>
  </div>
</div>