<?php
/**
 * @var \Illuminate\Pagination\LengthAwarePaginator $collection (Status)
 * @var array $successMessages
 * @var array $dangerMessages
 * @var array $filter
 */ 
?>

<!-- default layout theme -->
@extends('layouts.admin')

<!-- top search bar -->
@include('statuses.search', array(
  'filter'   => $filter
)) 

<!-- header and breadcrumbs -->
@include('statuses.page-header')

@section('content')
  <!-- success messages component -->
  @includeWhen($successMessages, 'components.dismissable-notice-panels.notice-info', array(
    'messages' => $successMessages
  ))

  <!-- danger messages component -->
  @includeWhen($dangerMessages, 'components.dismissable-notice-panels.notice-danger', array(
    'messages' => $dangerMessages
  )) 

  <div class="row">
    <div class="col-md-12">
      <div class="panel shadow">
        <div class="panel-body">
          <div class="table-responsive" style="margin-top: -1px;">
            <table class="table table-striped table-success">
              <thead>
              <tr>
                <th class="text-center border-right" style="width: 1%;">#</th>
                <th>Descri&ccedil;&atilde;o</th>
                <th>Criado em</th>
                <th>Atualizado em</th>
                <th class="text-center" style="width: 12%;"></th>
              </tr>
              </thead>
              <tbody>
              @if (count($collection))
                @foreach ($collection as $row)
                <tr>
                  <td class="text-center border-right">{{ $row->id }}</td>
                  <td><span>{{ $row->description }}</span></td>
                  <td>{{ $row->created_at_br }}</td>
                  <td>{{ $row->updated_at_br }}</td>
                  <td class="text-center">
                    <a href="/admin/status/{{ $row->id }}" class="btn btn-success btn-xs" title="Exibir Registro"><i class="fa fa-eye"></i></a>
                    <a href="/admin/status/update/{{ $row->id }}" class="btn btn-primary btn-xs" title="Editar Registro"><i class="fa fa-pencil"></i></a>
                    <a href="/admin/status/delete/{{ $row->id }}" class="btn btn-danger btn-xs js-confirm-to-remove" title="Remover Registro"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
                @endforeach
              @else 
                <tr>
                  <td colspan="5"><b>Nenhum registro encontrado<b></td>
                </tr>
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- paginator -->
  {{ $collection->appends(['filter' => $filter])->links() }}
@endsection
