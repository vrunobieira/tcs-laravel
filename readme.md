## Informações sobre o projeto
Este projeto foi desenvolvido utilizando as seguintes diretivas
- Laravel >=5.4
- PHP 7.1.27
- Apache 2.4.7
- Mysql v14.14 Distrib 5.7.25
- Frontend : bootstrap e jquery

- Os codigos do backend (PHP) foram escritos sob as normas da PSR-2 (https://www.php-fig.org/psr/psr-2/)
- Editor configurado para utilizar espaços ao invez de TAB
	- backend: 4 espaços
	- frontend: 2 espaços

## Detalhes de configuração
Para configurar o projeto local:

1. Configurar um virtual host apontando para a pasta public do projeto
`
<VirtualHost *:80>
    DocumentRoot /var/www/html/api-laravel/public
    ServerName api-laravel
</VirtualHost>
`

2. Habilitar o uso do htaccess do projeto para utilização de urls amigaveis
`
<Directory /var/www/html/api-laravel/>
	allow from all
	Order allow,deny
	Options +Indexes +FollowSymLinks
	Require all granted
	AllowOverride all
</Directory>
` 

(* não esquecer de adicionar o nome do virtual host em /etc/hosts. As configs acima são de utilização do apache)

3. Na raiz do projeto, existe o arquivo .env, o qual contem os dados do projeto (database, host, user, password, url do projeto...)

4. Na pasta database esta o arquivo `database.sql`, o qual contem criação do schema, das tabelas, e do usuario padrao para acessar a aplicação.
Basta dar um source no arquivo q o banco sobe certinho.

5. As controllers estão em /app/Http/Controllers

6. As models estão em /app/Models

7. As views estão em /resources/view. As views estão no formato blade.template (padrão de templates do laravel)

8. CSS e JS estão em /public (css|js)
- scripts referentes aos modulos do sistema estão dentro da pasta `modules` (em suas respectivas pastas raizes)

9. As regras para configuração de rotas da aplicação estao em /routes/web.php

10. A configuração para o serviço de autenticação é feita no arquivo /config/auth.php::70
O laravel utiliza a model que possui a trait de auth para executar o login.

## Uso geral da ferramenta
A tela de login esta disponivel na rota /login
User: admin@tcs.com.br
senha: qwe123@

Ao acessar a aplicação você será redirecionado para a tela do simulador.
No primeiro acesso esta tela estará vazia, pois nenhum evento foi registrado.
Siga para o modulo de cadastro de status.

Ao lado esquerdo se encontra o menu de navegação
- Status
- Maquinas
- Simulador

Para o cadastro de maquinas é necessario o cadastro dos status.
Para o cadastro de eventos é necessario o cadastro de maquinas.

O botão para novo cadastro de registros sempre está localizado ao lado do titulo da pagina (icone de uma folha de papel em branco).
O titulo da pagina fica em baixo da barra de pesquisa.

Apos cadastrar um novo registro, atualizar um registro existente ou remover um registro, você será direcionado para a tela inicial do modulo.
Os icones para editar ou remover registros são mostrados na grid de resultados da tela inicial do modulo.

No cabeçalho da pagina é mostrado mensagens de erro ou sucesso após cada transação. Basta clicar em cima da mensagem que ela irá desaparecer.

Os registros de Maquina e Status são removidos lógicamente apenas, para manter a integridade dos dados. (Soft Delete: recurso do framework)
Os registros dos eventos podem ser excluidos fisicamente.

Os registros de eventos possuem uma regra de validação de uniqueness para o id da maquina (engine_id).
Portanto apenas um registro de evento pode ser cadastrado para cada maquina.
Não há regra quanto a quantidade de vezes que o mesmo pode ser editado.

Na tela inicial do simulador sera mostrado os dados das maquinas cadastradas que possuem um evento vinculado a elas.
A cada x segundos cada registro irá atualizar de acordo com sua periodicidade definida (sempre em segundos).

Bom, é isso.
Abaixo está o readme padrão que vem com o framework para informações adicionais sobre a ferramenta


--
Vrunobieira <vrunobieira@gmail.com>
--


<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell):

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Pulse Storm](http://www.pulsestorm.net/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
