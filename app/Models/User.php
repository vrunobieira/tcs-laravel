<?php
namespace App\Models;

use App\Models\Custom\BDSModel;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * Model para tabela `users`
 */
class User extends BDSModel 
{
    /**
     * Traits for soft deletions
     */
    use SoftDeletes;

    /**
     * Class constructor
     * 
     * @param array $attributes 
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Call parent boot and applies query global scopes
     * 
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function($model) {
            $model->serializePassword();
        });
    }

    /**
     * The attributes that are mass assignable
     * 
     * @var array
     */
    protected $guarded = [
        'id', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Validation rules
     * 
     * @return array
     */
    public function rules() : array
    {
        return [
            'name'              => 'required|string|max:255',
            'phone'             => ['required', 'email', 'max:255', $this->getUniquenessRule()],
            'input_password'    => 'sometimes|string|max:30|same:password_confirmation'
        ];
    }

    /**
     * Fetch a record by id
     * - Uses withTrashed
     *
     * @param  int    $id 
     * @throws \Exception Record not found
     * @return User
     */
    public static function findById(int $id) : User
    {
        $user = User::withTrashed()->where('id', $id)->first();

        if (!$user instanceof User) {
            throw new \Exception ("Houve um erro ao buscar o registro informado.");
        }

        return $user;
    }

    /**
     * Accessor for name property
     * 
     * @return string
     */
    public function getNameAttribute() : string
    {
        return ucwords(strtolower($this->attributes['name'] ?? ''));
    }

    /**
     * Checks whether the record is soft-deleted or not, and returns it
     * current status: active | inactive
     * 
     * @return string {Ativo | Bloqueado}
     */
    public function getStatusAttribute() : string
    {
        return $this->trashed() ? 'Bloqueado' : 'Ativo';
    }

    /**
     * Checks whether the record is an admin user and returns Yes or Not
     * 
     * @return string {Sim | Não}
     */
    public function getIsAdminVerboseAttribute() : string
    {
        return $this->isAdmin() ? 'Sim' : 'Não';
    }

    /**
     * Accessor
     * Checks whether the record is not trashed
     * 
     * @return bool
     */
    public function getIsActiveAttribute() : bool
    {
        return !$this->trashed();
    }

    /**
     * created_at format : d/m/Y H:i:s
     * 
     * @return string
     */
    public function getCreatedAtBrAttribute() : string 
    {
        $datetime = new \DateTime($this->created_at);

        return $datetime->format('d/m/Y H:i:s');
    }

    /**
     * updated_at format : d/m/Y H:i:s
     * 
     * @return string
     */
    public function getUpdatedAtBrAttribute() : string 
    {
        $datetime = new \DateTime($this->updated_at);

        return $datetime->format('d/m/Y H:i:s');
    }

    /**
     * Checks whether the current record belongs to the authenticated session user
     * 
     * @return boolean
     */
    public function isAuthUser() : bool
    {
        return (Auth::check() && $this->id === Auth::user()->id);
    }

    /**
     * Retrieves rule for unique-validation
     */
    private function getUniquenessRule()
    {
        return $this->id == null
            ? Rule::unique($this->table)
            : Rule::unique($this->table)->ignore($this->id);
    }

    /**
     * Internal validation to prevent non authenticated users and/or non admin users
     * to remove records from user table on database
     * 
     * Checks whether the authenticated user is admin
     * 
     * @return bool
     */
    private function isAuthUserAdmin() : bool
    {
        return (Auth::check() && Auth::user()->is_admin);
    }

    /**
     * Serializa a senha do usuario
     * 
     * @return self
     */
    private function serializePassword() : self
    {
        if (!$this->hasErrors()) {
            if (isset($this->input_password) && !empty($this->input_password)) {
                $this->password = bcrypt($this->input_password);

                unset($this->input_password);
                unset($this->password_confirmation);
            }
        }

        return $this;
    }
    
}
