<?php

namespace App\Models;

use App\Models\Custom\BDSModel;
use Illuminate\Validation\Rule;

class Simulator extends BDSModel
{
    /**
     * Nome da tabela
     * 
     * @var string
     */
    protected $table = 'simulator';

    /**
     * Lista de propriedades da model que esta permitido o mass-assignment
     * 
     * @var array
     */
    protected $fillable = ['engine_id', 'frequency'];

    /**
     * Desabilitando timestamps para a tabela
     * 
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Regras de validação
     * 
     * @return array
     */
    public function rules() : array 
    {
        return [
            'engine_id' => ['required', 'exists:engines,id', $this->getUniquenessRule()],
            'frequency' => 'required|integer|between:1,60'
        ];
    }

    /**
     * Relation has-one: Engine
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function engine() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('\App\Models\Engine');
    }

    /**
     * Consulta todos os registros de maquinas e retorna uma collection paginada
     * 
     * @param  array  $filter 
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public static function fetchAll() : \Illuminate\Database\Eloquent\Collection
    {
        $query = Simulator::select('id', 'engine_id', 'frequency')
            ->with(['engine' => function($engineQuery) {
                $engineQuery
                    ->select('id', 'description', 'status_id', 'updated_at', 'deleted_at')
                    ->with(['status' => function($statusQuery) {
                        $statusQuery
                            ->select('id', 'description')
                            ->withTrashed();
                    }])
                    ->withTrashed();
            }]);

        return $query->get();
    }

    /**
     * Retorna a propriedade $frequency convertida para milisegundos
     * 
     * @return int
     */
    public function getFrequencyMsAttribute() : int
    {
        return intval($this->frequency ?? 0) * 1000;
    }

    /**
     * Getter para o atributo Engine::$description
     * 
     * @return string
     */
    public function getEngineDescriptionAttribute() : string
    {
        return $this->engine->description ?? '';
    }

    /**
     * Getter para a descrição do status da maquina (Engine)
     * 
     * @return string
     */
    public function getEngineStatusNameAttribute() : string
    {
        return $this->engine->status_name ?? '';
    }

    /**
     * Getter para a propriedade Engine::$updated_at
     * 
     * @return string
     */
    public function getEngineUpdatedAtAttribute() : string
    {
        return $this->engine->updated_at_br ?? '';
    }

    /**
     * Mutator para a propriedade status_id
     * Ao chamar o setter da propriedade faz o cast do valor 
     * do status selecionado para int
     * 
     * @param mixed $statusId
     */
    public function setStatusIdAttribute($statusId) : self
    {
        $this->attributes['status_id'] = intval($statusId);

        return $this;
    }

    /**
     * Accessor: Status::$description
     * 
     * @return string
     */
    public function getStatusNameAttribute() : string
    {
        return $this->status->description ?? '';
    }

    /**
     * Accessor para a propriedade created_at
     * 
     * @return string d/m/Y H:i:s
     */
    public function getCreatedAtBrAttribute() : string 
    {
        $datetime = new \DateTime($this->created_at);

        return $datetime->format('d/m/Y H:i:s');
    }

    /**
     * Accessor para a propriedade updated_at
     * 
     * @return string d/m/Y H:i:s
     */
    public function getUpdatedAtBrAttribute() : string 
    {
        $datetime = new \DateTime($this->updated_at);

        return $datetime->format('d/m/Y H:i:s');
    }

    /**
     * Regra custom para validacao de uniqueness
     *
     * @return Rule
     */
    private function getUniquenessRule() : \Illuminate\Validation\Rules\Unique
    {
        return $this->id == null
            ? Rule::unique($this->table)
            : Rule::unique($this->table)->ignore($this->id);
    }
}
