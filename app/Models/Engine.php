<?php

namespace App\Models;

use App\Models\Custom\BDSModel;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\SoftDeletes;

class Engine extends BDSModel
{
    /**
     * Trait para habilitar soft-delete
     */
    use SoftDeletes;

    /**
     * Lista de propriedades da model que esta permitido o mass-assignment
     * 
     * @var array
     */
    protected $fillable = ['description', 'status_id'];

    /**
     * Regras de validação
     * 
     * @return array
     */
    public function rules() : array 
    {
        return [
            'description' => 'required|string|max:255',
            'status_id'   => 'required|exists:statuses,id|integer'
        ];
    }

    /**
     * Relation belongs-to: Status
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('\App\Models\Status');
    }

    /**
     * Consulta todos os registros de maquinas e retorna uma collection paginada
     * 
     * @param  array  $filter 
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public static function allByFilter(array $filter = []) : \Illuminate\Pagination\LengthAwarePaginator
    {
        $query = Engine::select('id', 'description', 'status_id', 'created_at', 'updated_at')
            ->with(['status' => function($query) {
                $query->select('id', 'description')->withTrashed();
            }]);

        if (isset($filter['status']) && intval($filter['status']) > 0) {
            $query->where('status_id', $filter['status']);
        }

        if (isset($filter['search']) && !empty($filter['search'])) {
            $query->where('description', 'like', '%' . $filter['search'] . '%');
        }

        return $query->paginate(10);
    }

    /**
     * Mutator para a propriedade status_id
     * Ao chamar o setter da propriedade faz o cast do valor 
     * do status selecionado para int
     * 
     * @param mixed $statusId
     */
    public function setStatusIdAttribute($statusId) : self
    {
        $this->attributes['status_id'] = intval($statusId);

        return $this;
    }

    /**
     * Accessor: Status::$description
     * 
     * @return string
     */
    public function getStatusNameAttribute() : string
    {
        return $this->status->description ?? '';
    }

    /**
     * Accessor para a propriedade created_at
     * 
     * @return string d/m/Y H:i:s
     */
    public function getCreatedAtBrAttribute() : string 
    {
        $datetime = new \DateTime($this->created_at);

        return $datetime->format('d/m/Y H:i:s');
    }

    /**
     * Accessor para a propriedade updated_at
     * 
     * @return string d/m/Y H:i:s
     */
    public function getUpdatedAtBrAttribute() : string 
    {
        $datetime = new \DateTime($this->updated_at);

        return $datetime->format('d/m/Y H:i:s');
    }
}
