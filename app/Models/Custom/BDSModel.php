<?php

namespace App\Models\Custom;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator as FacadeValidator;
use Illuminate\Validation\Validator;

/**
 * This class implements validations methods used by it called class
 *
 * The called class must implement a public customRules method, 
 * which must return a key pair array used by Illuminate\Support\Facades\Validator
 * Check Laravel documentation page for facade validator
 *
 * To create custom validation rules, your model must contain a method customRules,
 * which will return a key pair array values. 
 * Its key should be the property to be validated.
 * Its value should be an array, with method names inside its class
 *
 * Sample return for customRules method: 
 * 
 * [
 *   'field1' => ['validationMethod1', 'validationMethod2'],
 *   'field2' => ['validationMethod2']
 * ]
 * 
 * - In this sample, 'validationMethod1' and 'validationMethod2' must be declared on its called class 
 * and should return an string with the error, or an empty string
 *
 * @author Bruno Vieira <vrunobieira@gmail.com>
 */
abstract class BDSModel extends Model
{

    /**
     * Validator object
     * 
     * @var \Illuminate\Validation\Validator
     */
    private $validator = null;

    /**
     * Checks whether the validator was called previously by the model
     * 
     * @var boolean
     */
    private $didValidatorRun = false;

    /**
     * Call parent constructor an dispatch validator events
     * 
     * @param array $attributes 
     */
    public function __construct(array $attributes = [])
    {
        // run parent constructor
        parent::__construct($attributes);

        // register the validator within the class
        $this->_registerValidator();
    }

    /**
     * Accessor for getErrorMessages()
     * 
     * @return array
     */
    public function getErrorMessagesAttribute() : array
    {
        return $this->getErrorMessages();
    }

    /**
     * Accessor for hasErrors()
     * 
     * @return bool
     */
    public function getHasErrorsAttribute() : bool
    {
        return $this->hasErrors();
    }

    /**
     * Run model validations
     *
     * @return bool
     */
    public function validate() : bool
    {
        // register the validator within the class
        $this->_registerValidator();

        // change flag to inform the validator has run by public call
        $this->_setDidValidate();

        // returns true if does not contain errors
        return !$this->hasErrors();
    }

    /**
     * Checks whether the validation has run and the model has errors
     * 
     * @var boolean
     */
    public function hasErrors() : bool
    {
        return $this->_getDidValidatorRun()
            ? $this->_getValidator()->fails()
            : false;
    }

    /**
     * Retrieves error messages list
     * 
     * @return array
     */
    public function getErrorMessages() : array
    {
        return $this->hasErrors()
            ? $this->_getValidator()->errors()->all()
            : [];
    }

    /**
     * Setter for BDSModel::$validator
     * 
     * @param Illuminate\Validation\Validator $validator 
     * @return void
     */
    private function _setValidator(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Getter for BDSModel::$validator
     * 
     * @return Illuminate\Validation\Validator
     */
    private function _getValidator() : Validator
    {
        return $this->validator;
    }

    /**
     * Start new validator and binds it to this class
     * 
     * @return void
     */
    private function _initValidator()
    {
        $validator = FacadeValidator::make($this->getAttributes(), $this->rules());

        $this->_setValidator($validator);
    }

    /**
     * Getter for $validator
     * 
     * @return Illuminate\Validation\Validator
     */
    protected function getValidator() : Validator
    {
        return $this->validator;
    }

    /**
     * Run additional validations registered by called class
     * 
     * @return void
     */
    private function _registerCustomRules()
    {
        // checks whether the customRules method is declared on called class
        // otherwise bypass register event
        if (!method_exists(get_called_class(), 'customRules')) 
            return;

        // get validator object
        $validator = $this->_getValidator();

        // applies custom validations on after-validate event
        $validator->after(function ($validator) {

            // called class namespace
            $calledClass = get_called_class();

            // iterate over custom rules
            foreach ($this->customRules() as $property => $rules) {

                // iterate over validate methods list declared at called class 
                foreach ($rules as $rule) {

                    // checks whether the rule name set in custom rules 
                    // is correctly declared at called class as function
                    if (!method_exists($calledClass, $rule))
                        throw new \Exception(sprintf('Method %s does not exist at %s. Check your custom rules configuration', $rule, $calledClass));

                    // run custom validation rule
                    $errorMessage = call_user_func_array([$this, $rule], []);

                    // add error message
                    if (!empty($errorMessage))
                        $validator->errors()->add($property, $errorMessage);
                }
            }
        });
    }

    /**
     * Register validator and its custom rules to this class
     * 
     * @return void
     */
    private function _registerValidator()
    {
        // run validator with main rules
        $this->_initValidator();

        // register custom rules
        $this->_registerCustomRules();
    }

    /**
     * Update flag to inform that the method validate has been called
     *
     * @return void
     */
    private function _setDidValidate()
    {
        $this->didValidatorRun = true;
    }

    /**
     * Checks whether the validator has run by validate event
     * 
     * @return bool
     */
    private function _getDidValidatorRun() : bool
    {
        return $this->didValidatorRun;
    }

}
