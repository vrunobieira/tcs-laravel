<?php

namespace App\Models;

use App\Models\Custom\BDSModel;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends BDSModel
{
    /**
     * Trait para habilitar soft-delete
     */
    use SoftDeletes;

    /**
     * Lista de propriedades da model que esta permitido o mass-assignment
     * 
     * @var array
     */
    protected $fillable = ['description'];

    /**
     * Regras de validação
     * 
     * @return array
     */
    public function rules() : array 
    {
        return [
            'description' => 'required|string|max:255'
        ];
    }

    /**
     * Consulta todos os registros de Status e retorna uma collection paginada
     * 
     * @param  array  $filter 
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public static function allByFilter(array $filter = []) : \Illuminate\Pagination\LengthAwarePaginator
    {
        $query = Status::select('id', 'description', 'created_at', 'updated_at');

        if (isset($filter['search']) && !empty($filter['search'])) {
            $query->where('description', 'like', '%' . $filter['search'] . '%');
        }

        return $query->paginate(10);
    }

    /**
     * Accessor para a propriedade created_at
     * 
     * @return string d/m/Y H:i:s
     */
    public function getCreatedAtBrAttribute() : string 
    {
        $datetime = new \DateTime($this->created_at);

        return $datetime->format('d/m/Y H:i:s');
    }

    /**
     * Accessor para a propriedade updated_at
     * 
     * @return string d/m/Y H:i:s
     */
    public function getUpdatedAtBrAttribute() : string 
    {
        $datetime = new \DateTime($this->updated_at);

        return $datetime->format('d/m/Y H:i:s');
    }
}
