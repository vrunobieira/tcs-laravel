<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class AuthUser extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    /**
     * Traits for authentication and soft deletions
     */
    use Authenticatable, Authorizable, SoftDeletes;

    /**
     * $table 
     * 
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable
     * 
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Disable deletion through this model
     * 
     * @return bool {false}
     */
    public function delete() : bool { return false; }

}
