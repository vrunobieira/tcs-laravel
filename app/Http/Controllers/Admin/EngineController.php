<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Models\Engine;
use App\Models\Status;

/**
 * Controller para o modulo de maquinas
 */
class EngineController extends Controller 
{
    /**
     * Index para o modulo de maquinas
     * 
     * @param Request $request
     * @return HTML engines/index
     */
    public function index(Request $request)
    {
        $filter = $this->getFilterParams($request);
        $collection = Engine::allByFilter($filter);
        $statuses = Status::allByFilter();

        $successMessages = $request->session()->pull('engine.success', []);
        $dangerMessages = $request->session()->pull('engine.danger', []);

        return view('engines.index', [
            'successMessages' => $successMessages,
            'dangerMessages'  => $dangerMessages,
            'filter'          => $filter,
            'collection'      => $collection,
            'statuses'        => $statuses
        ]);
    }

    /**
     * Form para criar novo registro de maquina
     * 
     * @param \Illuminate\Http\Request $request
     * @return HTML engines/create
     */
    public function create(Request $request)
    {
        $engine = new Engine;
        $statuses = Status::allByFilter();

        $response = $this->dispatchPersist($engine, $request);

        if (null !== $response) {
            return $response;
        }

        return view('engines.create', [
            'engine'    => $engine,
            'statuses'  => $statuses
        ]);
    }

    /**
     * Form para editar um registro de maquina
     * 
     * @param \Illuminate\Http\Request $request
     * @param integer $id
     * @return HTML engines/update
     */
    public function update(Request $request, $id)
    {
        $engine = Engine::findOrFail($id);
        $statuses = Status::allByFilter();

        $response = $this->dispatchPersist($engine, $request);

        if (null !== $response) {
            return $response;
        }

        return view('engines.update', [
            'engine'    => $engine,
            'statuses'  => $statuses
        ]);
    }

    /**
     * Action para remover um registro de maquina
     * 
     * @param \Illuminate\Http\Request $request
     * @param integer $id
     */
    public function delete(Request $request, $id)
    {
        $engine = Engine::findOrFail($id);

        try {
            $engine->delete();
            $request->session()->push(
                'engine.success', 'Dados removidos com sucesso'
            );
        } catch (\Exception $e) {
            $request->session()->push(
                'engine.danger', sprintf(
                    'Houve um erro ao remover este registro. ERROR: %s', 
                    $e->getMessage()
                )
            );
        }

        return redirect('/admin/engines');
    }

    /**
     * Tela para exibir o registro de maquina
     * 
     * @param integer $id
     * @return HTML engines/show
     */
    public function show($id)
    {
        return view('engines.show', [
            'engine' => Engine::findOrFail($id)
        ]);
    }

    /**
     * Popula a model Engine com os dados da request e tenta executar o persist.
     * Em caso de sucesso retorna uma instance de RedirectResponse
     * 
     * @param Engine  &$engine 
     * @param Request $request 
     * @return RedirectResponse|null
     */
    private function dispatchPersist(
        Engine &$engine, 
        Request $request
    ) {

        if ($request->isMethod('post')) {
            $requestData = $request->input('engine', []);

            $engine
                ->fill($requestData)
                ->setStatusIdAttribute($requestData['status_id'] ?? null);

            if ($engine->validate()) {
                try {
                    $engine->save();

                    $request->session()->push(
                        'engine.success', 
                        'Dados salvos com sucesso!'
                    );
                } catch(\Exception $e) {
                    $request->session()->push(
                        'engine.danger', sprintf('Houve um erro ao salvar as informações. ERROR: %s', $e->getMessage())
                    );
                }

                return redirect('/admin/engines');
            }
        }

        return null;
    }

    /**
     * Retorna os dados dos filtros de pesquisa
     * 
     * @param  Request $request
     * @return array
     *             - status: int
     *             - search: string
     */
    private function getFilterParams(Request $request) : array
    {
        return [
            'status' => intval($request->input('filter.status', 0)),
            'search' => $request->input('filter.search', '')
        ];
    }
}