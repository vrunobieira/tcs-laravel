<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Models\Engine;
use App\Models\Simulator;
use App\Models\Status;

/**
 * Controller para o simulador de eventos
 */
class SimulatorController extends Controller 
{
    /**
     * Tela principal do simulador de eventos
     * 
     * @param Request $request
     * @return HTML simulator/index
     */
    public function index(Request $request)
    {
        $collection = Simulator::fetchAll();

        $successMessages = $request->session()->pull('simulator.success', []);
        $dangerMessages = $request->session()->pull('simulator.danger', []);

        return view('simulator.index', [
            'successMessages' => $successMessages,
            'dangerMessages'  => $dangerMessages,
            'collection'      => $collection
        ]);
    }

    /**
     * Form para criar novo registro de evento
     * 
     * @param \Illuminate\Http\Request $request
     * @return HTML simulator/create
     */
    public function create(Request $request)
    {
        $simulator = new Simulator;
        $engines = Engine::allByFilter();

        $response = $this->dispatchPersist($simulator, $request);

        if (null !== $response) {
            return $response;
        }

        return view('simulator.create', [
            'simulator' => $simulator,
            'engines'   => $engines
        ]);
    }

    /**
     * Form para editar um registro de evento
     * 
     * @param \Illuminate\Http\Request $request
     * @param integer $id
     * @return HTML simulator/update
     */
    public function update(Request $request, $id)
    {
        $simulator = Simulator::findOrFail($id);
        $engines = Engine::allByFilter();

        $response = $this->dispatchPersist($simulator, $request);

        if (null !== $response) {
            return $response;
        }

        return view('simulator.update', [
            'simulator' => $simulator,
            'engines'   => $engines
        ]);
    }

    /**
     * Action para remover um registro de simulator
     * 
     * @param \Illuminate\Http\Request $request
     * @param integer $id
     */
    public function delete(Request $request, $id)
    {
        $simulator = Simulator::findOrFail($id);

        try {
            $simulator->delete();
            $request->session()->push(
                'simulator.success', 'Dados removidos com sucesso'
            );
        } catch (\Exception $e) {
            $request->session()->push(
                'simulator.danger', sprintf(
                    'Houve um erro ao remover este registro. ERROR: %s', 
                    $e->getMessage()
                )
            );
        }

        return redirect('/admin/simulator');
    }

    /**
     * Atualiza o status de uma maquina registrada com um evento
     * para um status gerado aleatoriamente (de acordo com os status cadastrados e ATIVOS)
     *
     * Action disponivel apenas para requisicoes ajax
     * 
     * @param Request $request Objeto da request
     * @param int     $id      Id da maquina
     * @return []
     * @throws Exception se findOrFail falhar
     */
    public function updateEngine(Request $request, $id)
    {
        if ($request->ajax()) {
            $engine = Engine::findOrFail($id);
            $status = Status::inRandomOrder()->first();

            if ($status instanceof Status) {
                try {
                    $engine->status_id = $status->id;
                    $engine->save();

                    return [
                        'engine_status_name' => $status->description,
                        'engine_updated_at' => $engine->updated_at_br
                    ];
                } catch (\Exception $e) {
                    ;
                }
            }

            return [];
        }
    }

    /**
     * Popula a model Simulator com os dados da request e tenta executar o persist.
     * Em caso de sucesso retorna uma instance de RedirectResponse
     * 
     * @param Simulator &$simulator 
     * @param Request $request 
     * @return RedirectResponse|null
     */
    private function dispatchPersist(
        Simulator &$simulator, 
        Request $request
    ) {

        if ($request->isMethod('post')) {
            $simulator->fill($request->input('simulator', []));

            if ($simulator->validate()) {
                try {
                    $simulator->save();

                    $request->session()->push(
                        'simulator.success', 
                        'Dados salvos com sucesso!'
                    );
                } catch(\Exception $e) {
                    $request->session()->push(
                        'simulator.danger', sprintf('Houve um erro ao salvar as informações. ERROR: %s', $e->getMessage())
                    );
                }

                return redirect('/admin/simulator');
            }
        }

        return null;
    }
}