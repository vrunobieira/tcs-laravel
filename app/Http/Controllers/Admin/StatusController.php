<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Models\Status;

/**
 * Controller para o modulo de status
 */
class StatusController extends Controller 
{
    /**
     * Index para o modulo de status
     * 
     * @param Request $request
     * @return HTML statuses/index
     */
    public function index(Request $request)
    {
        $filter = $this->getFilterParams($request);
        $collection = Status::allByFilter($filter);

        $successMessages = $request->session()->pull('status.success', []);
        $dangerMessages = $request->session()->pull('status.danger', []);

        return view('statuses.index', [
            'successMessages' => $successMessages,
            'dangerMessages'  => $dangerMessages,
            'filter'          => $filter,
            'collection'      => $collection
        ]);
    }

    /**
     * Form para criar novo registro de status
     * 
     * @param \Illuminate\Http\Request $request
     * @return HTML statuses/create
     */
    public function create(Request $request)
    {
        $status = new Status;

        $response = $this->dispatchPersist($status, $request);

        if (null !== $response) {
            return $response;
        }

        return view('statuses.create', [
            'status' => $status
        ]);
    }

    /**
     * Form para editar um registro de status
     * 
     * @param \Illuminate\Http\Request $request
     * @param integer $id
     * @return HTML statuses/update
     */
    public function update(Request $request, $id)
    {
        $status = Status::findOrFail($id);

        $response = $this->dispatchPersist($status, $request);

        if (null !== $response) {
            return $response;
        }

        return view('statuses.update', [
            'status' => $status
        ]);
    }

    /**
     * Action para remover um registro de status
     * 
     * @param \Illuminate\Http\Request $request
     * @param integer $id
     */
    public function delete(Request $request, $id)
    {
        $status = Status::findOrFail($id);

        try {
            $status->delete();
            $request->session()->push(
                'status.success', 'Dados removidos com sucesso'
            );
        } catch (\Exception $e) {
            $request->session()->push(
                'status.danger', sprintf(
                    'Houve um erro ao remover este registro. ERROR: %s', 
                    $e->getMessage()
                )
            );
        }

        return redirect('/admin/statuses');
    }

    /**
     * Tela para exibir o registro de status
     * 
     * @param integer $id
     * @return HTML statuses/show
     */
    public function show($id)
    {
        return view('statuses.show', [
            'status' => Status::findOrFail($id)
        ]);
    }

    /**
     * Popula a model status com os dados da request e tenta executar o persist.
     * Em caso de sucesso retorna uma instance de RedirectResponse
     * 
     * @param Status  &$status 
     * @param Request $request 
     * @return RedirectResponse|null
     */
    private function dispatchPersist(
        Status &$status, 
        Request $request
    ) {

        if ($request->isMethod('post')) {
            $status->fill($request->input('status', []));

            if ($status->validate()) {
                try {
                    $status->save();

                    $request->session()->push(
                        'status.success', 
                        'Dados salvos com sucesso!'
                    );
                } catch(\Exception $e) {
                    $request->session()->push(
                        'status.danger', sprintf('Houve um erro ao salvar as informações. ERROR: %s', $e->getMessage())
                    );
                }

                return redirect('/admin/statuses');
            }
        }

        return null;
    }

    /**
     * Retorna os dados dos filtros de pesquisa
     * 
     * @param  Request $request
     * @return array
     *             - search: string
     */
    private function getFilterParams(Request $request) : array
    {
        return [
            'search' => $request->input('filter.search', '')
        ];
    }
}