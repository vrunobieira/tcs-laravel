
class IndexController {
  /**
   * Registra os eventos para atualizacao do status das maquinas
   * @return {void}
   */
  init() {
    this.handleSimulatorUpdate();
  }

  /**
   * Registra o evento de atualizacao para cada maquina cadastrada com um evento
   * @return {void}
   */
  handleSimulatorUpdate() {
    var self = this;
    let containers = $(".js-engine_container");

    containers.each(function(i) {
      self.bindUpdateEventTo(this);
    });
  }

  /**
   * Dispara o evento referente ao container com a maquina para 
   * executar a cada x segundos (de acordo com a configuracao do evento)
   * @param  {jQuery} container O componente com os dados da maquina e do evento
   * @return {void}
   */
  bindUpdateEventTo(container) {
    var self = this;
    let frequency = parseInt($(container).attr("data-frequency_ms"));
    let engineId = parseInt($(container).attr("data-id"));

    setInterval(function(){ 
      self.callAjaxUpdate(engineId);
    }, frequency);
  }

  /**
   * Executa a chamada ajax para atualizar a maquina pelo ID
   * @param  {int} engineId ID da maquina que sera atualizada
   * @return {void}
   */
  callAjaxUpdate(engineId) {
    var self = this;

    try {
      $.get(`/admin/simulator/update-engine/${engineId}`, {}, function(data) {
        self.updateContainer(
          engineId,
          self.parseJsonData(data) 
        );
      }, 'json');
    } catch (e) {
      console.log(`Erro ao atualizar o status da maquina ${engineId}`);
    }
  }

  /**
   * Atualiza os dados da maquina no template
   * @param {int}  engineId Id da maquina
   * @param {json} data     Json com os dados da maquina
   * @return {void}
   */
  updateContainer(engineId, data) {
    let container = $(`.js-engine_container[data-id='${engineId}']`);

    $(container).find(".js-engine_status_name").html(data.engine_status_name);
    $(container).find(".js-engine_updated_at").html(data.engine_updated_at);
  }

  /**
   * Recupera o json de retorno da request e desencapsula os dados para que possa ser utilizado
   * para atualizar os dados da pagina
   * 
   * @param  {json} data
   * @return {json}
   */
  parseJsonData(data) {
    return {
      engine_status_name : data.hasOwnProperty("engine_status_name") ? data.engine_status_name : "",
      engine_updated_at : data.hasOwnProperty("engine_updated_at") ? data.engine_updated_at : ""
    }
  }
}

document.addEventListener('DOMContentLoaded', function() {
  let controller = new IndexController;
  controller.init();
});