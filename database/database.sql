SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema api_laravel
--
-- Database para o projeto 
-- -----------------------------------------------------

CREATE SCHEMA IF NOT EXISTS `api_laravel` DEFAULT CHARACTER SET utf8 ;

USE `api_laravel` ;

-- -----------------------------------------------------
-- Table `api_laravel`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_laravel`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL COMMENT 'Nome',
  `email` VARCHAR(255) NOT NULL COMMENT 'Email',
  `password` VARCHAR(255) NOT NULL COMMENT 'Senha do usuario',
  `remember_token` VARCHAR(255) NULL COMMENT 'Hash para utilização de cookie remember_me',
  `created_at` DATETIME NULL COMMENT 'Data de criação do registro',
  `updated_at` DATETIME NULL COMMENT 'Data da ultima atualização do registro',
  `deleted_at` DATETIME NULL COMMENT 'Data de remoção do registro',
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Dados dos usuarios com acesso na aplicação';

CREATE UNIQUE INDEX `email_UNIQUE` ON `api_laravel`.`users` (`email` ASC);

-- -----------------------------------------------------
-- Table `api_laravel`.`engines`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_laravel`.`engines` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status_id` INT NOT NULL COMMENT 'Status da maquina: statuses.id',
  `description` VARCHAR(255) NOT NULL COMMENT 'Descrição',
  `created_at` DATETIME NULL COMMENT 'Data de criação do registro',
  `updated_at` DATETIME NULL COMMENT 'Data da ultima atualização do registro',
  `deleted_at` DATETIME NULL COMMENT 'Data de remoção do registro',
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_statuses_status_id`
    FOREIGN KEY (`status_id`)
    REFERENCES `api_laravel`.`statuses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Dados das máquinas cadastradas';

-- -----------------------------------------------------
-- Table `api_laravel`.`statuses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_laravel`.`statuses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NOT NULL COMMENT 'Descrição',
  `created_at` DATETIME NULL COMMENT 'Data de criação do registro',
  `updated_at` DATETIME NULL COMMENT 'Data da ultima atualização do registro',
  `deleted_at` DATETIME NULL COMMENT 'Data de remoção do registro',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Status das máquinas';

-- -----------------------------------------------------
-- Table `api_laravel`.`simulator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_laravel`.`simulator` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `engine_id` INT NOT NULL COMMENT 'Maquina que sera atribuido o evento',
  `frequency` INT NOT NULL COMMENT 'Frequencia em segundos com que os eventos irão ocorrer para a maquina selecionada',
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_engines_engine_id`
    FOREIGN KEY (`engine_id`)
    REFERENCES `api_laravel`.`engines` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Configuração dos eventos de status para as máquinas';

CREATE UNIQUE INDEX `engine_id_UNIQUE` ON `api_laravel`.`simulator` (`engine_id` ASC);

--
-- Criação de novo usuario
--
-- O password é armazenado via bcrypt pela aplicação
-- password: qwe123@
--
INSERT INTO `users`
    (`name`, `email`, `password`, `created_at`, `updated_at`)
VALUES
    ('Admin', 'admin@tcs.com.br', '$2y$10$LHBYq7WDNP1jktA0zcgPWepR3txmvlPvhk4PUZE0s2.jp.bUvw9gm', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
