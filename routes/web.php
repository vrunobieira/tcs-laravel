<?php
/*
 * Rotas para a área restrita
 */
Route::namespace('Admin')->group(function() {
    /*
     * As rotas definidas abaixo pertencem a area restrita
     * e são executadas atravez do middleware de autenticação
     * Se o usuário não estiver autenticado na ferramenta, ele será redirecoinado para a tela de login 
     */
    Route::group(['middleware' => 'auth'], function() {

        /*
         * Rotas para o modulo de Status
         */
        Route::get('/admin/statuses', 'StatusController@index');
        Route::match(['get', 'post'], '/admin/status/create', 'StatusController@create');
        Route::match(['get', 'post'], '/admin/status/update/{id}', 'StatusController@update');
        Route::get('/admin/status/delete/{id}', 'StatusController@delete');
        Route::get('/admin/status/{id}', 'StatusController@show');

        /*
         * Rotas para o modulo de maquinas 
         */
        Route::get('/admin/engines', 'EngineController@index');
        Route::match(['get', 'post'], '/admin/engine/create', 'EngineController@create');
        Route::match(['get', 'post'], '/admin/engine/update/{id}', 'EngineController@update');
        Route::get('/admin/engine/delete/{id}', 'EngineController@delete');
        Route::get('/admin/engine/{id}', 'EngineController@show');

        /*
         * Rotas para o simulador de eventos 
         */
        Route::get('/admin/simulator', 'SimulatorController@index');
        Route::match(['get', 'post'], '/admin/simulator/create', 'SimulatorController@create');
        Route::match(['get', 'post'], '/admin/simulator/update/{id}', 'SimulatorController@update');
        Route::get('/admin/simulator/delete/{id}', 'SimulatorController@delete');
        Route::get('/admin/simulator/update-engine/{id}', 'SimulatorController@updateEngine');
    });
});

/*
 * Rotas de autenticação
 */
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');